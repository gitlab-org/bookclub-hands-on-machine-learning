# Hands-On Machine Learning with Skikit-Learn, Keras, and Tensorflow

- Book: https://www.oreilly.com/library/view/hands-on-machine-learning/9781492032632/
- Original Issue: https://gitlab.com/gitlab-com/book-clubs/-/issues/40
- Meeting agenda: https://docs.google.com/document/d/131r0w_EuUPrDG9Na5GsAR9PseW1hiNsJYMXQu8xUfjY
- Additional online material for the book: https://github.com/ageron/handson-ml2/

## Protocol

- Choose the chapters of the book you are most interested in. For beginners, chapters 1-4 are strongly recommended.
- On this bookclub repo, add yourself to the issue for that chapter. If it doesn't exist yet, [please create one](https://gitlab.com/gitlab-org/bookclub-hands-on-machine-learning/-/issues/new?issue%5Bmilestone_id%5D=)
- As we read the book, we can use the issue to ask questions or discuss ideas.
- Once the group has finished reading, the group creates a summary of learnings. So this will be mostly async.
- Code can also be added to the project for discussions. If you want to share code for a specific chapter please create a folder per chapter to put the files in.
- It would be great if we could do one chapter per week, but there's no time pressure.

## Sync recordings

| Date | Event | Recording | 
| ------ | ------ | ------ | 
| March 3rd 4PM UTC | Kick-off EMEA |  https://youtu.be/cZhVAO9DoPk  | 
| March 8th 8PM UTC | Kick-off AMER | https://youtu.be/UN30lF22XsM  |
